/****************************************************************************************
**
**  Author:
**      Bérenger ARNAUD
**
**  Project:
**      TaM
**
****************************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: homePage

    SilicaFlickable {
        anchors.fill: parent
        contentHeight: column.height

        // Why is this necessary?
        contentWidth: parent.width

        // PullDownMenu and PushUpMenu must be declared in SilicaFlickable, SilicaListView or SilicaGridView
        PullDownMenu {
            MenuItem {
                text: "Paramètres"
                onClicked: pageStack.push(Qt.resolvedUrl("Settings.qml"))
            }
            MenuItem {
                text: "Resa'TaM"
                onClicked: pageStack.push(Qt.resolvedUrl("ResaTaM.qml"))
            }
        }

        VerticalScrollDecorator {}
        Column {
            id: column
            spacing: Theme.paddingLarge
            width: parent.width

            PageHeader {
                title: "Transport Montpellier"
            }
            Image {
                source: "../images/TaM.png"
                width: parent.width / 2
                fillMode: Image.PreserveAspectFit
                anchors.horizontalCenter: parent.horizontalCenter
            }

            SectionHeader {
                text: "Plans"
            }
            Row {
                spacing: Theme.paddingLarge
                anchors.horizontalCenter: parent.horizontalCenter
                Button {
                    text: "Tramways"
                    enabled: false
                }
                Button {
                    text: "Montpellier"
                    enabled: false
                }
            }
            Row {
                spacing: Theme.paddingLarge
                anchors.horizontalCenter: parent.horizontalCenter
                Button {
                    text: "Agglomération"
                    enabled: false
                }
            }

            SectionHeader {
                text: "Voyage"
            }
            Row {
                spacing: Theme.paddingLarge
                anchors.horizontalCenter: parent.horizontalCenter
                Button {
                    id: routeButton
                    text: "Itinéraire"
                    enabled: false
                }
                Button {
                    text: "Horaire"
                    enabled: false
                }
            }
            Row {
                spacing: Theme.paddingLarge
                anchors.horizontalCenter: parent.horizontalCenter
                Button {
                    x: routeButton.x
                    text: "Resa'TaM"
                    onClicked: pageStack.push(Qt.resolvedUrl("ResaTaM.qml"))
                }
            }
        }
    }
}
