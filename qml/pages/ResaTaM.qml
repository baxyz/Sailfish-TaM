/****************************************************************************************
**
**  Author:
**      Bérenger ARNAUD
**
**  Project:
**      TaM
**
****************************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    SilicaFlickable {
        anchors.fill: parent

        PullDownMenu {
            MenuItem {
                text: qsTr("Reload")
                onClicked: webView.reload()
            }
        }

        PageHeader {
            id: headerLabel
            title: "Résa'TaM"
        }

        SilicaWebView {
            id: webView

            anchors {
                top: headerLabel.bottom
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }

            url: "http://212.99.16.101/RESATAM/identification.aspx?ReturnUrl=/RESATAM/default.aspx&AspxAutoDetectCookieSupport=1"
        }
    }
}





