/****************************************************************************************
**
**  Author:
**      Bérenger ARNAUD
**
**  Project:
**      TaM
**
****************************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: textInputPage

    property int textAlignment: Text.AlignLeft
    SilicaFlickable {
        anchors.fill: parent
        contentHeight: column.height + Theme.paddingLarge

        VerticalScrollDecorator {}
        Column {
            id: column
            width: parent.width

            PageHeader {
                title: "Paramètres"
            }

            SectionHeader {
                text: "Resa'TaM"
            }
            TextField {
                id: rtIdentInput
                width: parent.width
                label: "Identifiant"
                placeholderText: "Identifiant"
                focus: true
            }
            TextField {
                id: rtCodeInput
                width: parent.width
                inputMethodHints: Qt.ImhFormattedNumbersOnly
                label: "Code"
                placeholderText: "Code"
            }
        }
    }
}
