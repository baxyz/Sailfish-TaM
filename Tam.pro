# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = Tam

CONFIG += sailfishapp

SOURCES += src/Tam.cpp

OTHER_FILES += qml/Tam.qml \
    qml/cover/CoverPage.qml \
    rpm/Tam.changes.in \
    rpm/Tam.spec \
    rpm/Tam.yaml \
    translations/*.ts \
    Tam.desktop \
    qml/pages/ResaTaM.qml \
    qml/pages/Settings.qml \
    qml/pages/Home.qml \
    pics/TaM.png \
    qml/images/TaM.png \
    img/tam-200.png \
    img/tam-100.png \
    qml/images/tam-200.png \
    qml/pages/NetTram.qml

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n
TRANSLATIONS += translations/Tam-de.ts

